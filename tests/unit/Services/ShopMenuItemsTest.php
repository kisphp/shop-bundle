<?php

namespace Tests\Unit\ShopBundle\Services;

use Codeception\Test\Unit;
use Kisphp\ShopBundle\Services\Menu\ShopMenuItems;

/**
 * @group shop
 */
class ShopMenuItemsTest extends Unit
{
    public function test_menu_items_are_added()
    {
        $iterator = new \ArrayIterator();
        ShopMenuItems::getMenuItems($iterator);

        self::assertGreaterThan(0, count($iterator));
    }
}
