<?php

namespace Tests\Functional\ShopBundle\Controller;

/**
 * @group orders
 */
class OrderControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_orders_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/orders');
        $i->see('Customer orders', 'h3');
        $i->see('New order');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_order_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/orders');
        $i->see('Customer orders', 'h3');
        $i->see('New order');
        $i->see('Edit');
        $i->canSeeResponseCodeIs(200);
        $i->click('Edit');
        $i->see('Edit Order', 'h3');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/orders\/edit\/1/');
        $i->see('Order Items');
        $i->see('Product title');
        $i->see('Product code');
        $i->see('Recalculate total');
        $i->see('Process');
        $i->see('Cancel order');
        $i->click('Process');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/orders\/edit\/1/');
        $i->see('Edit');
        $i->click('Edit');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/orders\/edit\/item\/1/');
        $i->see('Edit Order', 'h3');
        $i->see('Quantity');
        $i->see('Shipping cost');
        $i->see('Add product to cart');
        $i->click('Add product to cart');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/orders\/edit\/item\/add/');
        $i->see('Quantity');
        $i->see('Id product');
        $i->cantSee('Price');
    }
}
