<?php

namespace Tests\Functional\ShopBundle\Controller;

/**
 * @group shop
 */
class CategoriesControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/category');
        $i->see('Categories', 'h3');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/category');
        $i->see('Categories', 'h3');
        $i->click('Edit');
        $i->see('Edit Category', 'h3');
        $i->see('Parent Category');
        $i->canSeeResponseCodeIs(200);
        $i->fillField('category_form[title]', 'Changed category');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->see('Successfull saved');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/category');
        $i->see('Edit');
        $i->click('Create');
        $i->see('Create Category');
        $i->fillField('category_form[title]', 'Demo Category');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/category\/edit/');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function add_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/category/add');
        $i->see('Create Category');
        $i->click('Save');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('This value should not be blank');
    }
}
