<?php

namespace Tests\Functional\ShopBundle\Controller;

use Codeception\Example;

/**
 * @group status
 * @group shop
 */
class StatusRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider statusUrlsProvider
     */
    public function status_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function statusUrlsProvider()
    {
        return [
            ['url' => '/shop/category/status'],
        ];
    }
}
