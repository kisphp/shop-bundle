<?php

namespace Tests\Functional\ShopBundle\Controller;

/**
 * @group prod
 */
class ProductsControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function listing_products_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/products');
        $i->see('Products', 'h3');
        $i->see('Product 1');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function edit_product_page(\FunctionalTester $i)
    {
        $i->amOnPage('/shop/products');
        $i->see('Products', 'h3');
        $i->see('Product 1');
        $i->see('Edit');
        $i->canSeeResponseCodeIs(200);
        $i->click('Edit');
        $i->see('Edit Product', 'h3');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/shop\/products\/edit/');
        $i->fillField('product_form[price_old]', 200);
        $i->click('Save');
        $i->canSee('Successfull saved');
        $i->canSeeResponseCodeIs(200);
    }
}
