<?php

namespace Tests\Functional\ShopBundle\Controller;

use Codeception\Example;

/**
 * @group delete
 * @group shop
 */
class DeleteRequestCest
{
    /**
     * @param \FunctionalTester $i
     *
     * @dataProvider deleteUrlsProvider
     */
    public function delete_change(\FunctionalTester $i, Example $urls)
    {
        $i->sendAjaxPostRequest($urls['url'], [
            'id' => 1,
        ]);
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @return array
     */
    public function deleteUrlsProvider()
    {
        return [
            ['url' => '/shop/category/delete'],
        ];
    }
}
