<?php

namespace Kisphp\ShopBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;
use Kisphp\Twig\IsSafeHtml;

/**
 * @todo move to bikeBundle
 */
class PriceFilter extends AbstractTwigFilter
{
    use IsSafeHtml;

    const CURRENCY = '&euro;';

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'price';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($price) {
            if ($price === 0) {
                return '';
            }

            return number_format($price / 100, 2) . ' ' . static::CURRENCY;
        };
    }
}
