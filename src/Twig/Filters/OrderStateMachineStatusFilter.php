<?php

namespace Kisphp\ShopBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;
use Kisphp\ShopBundle\StateMachine\Declarations\CustomerOrderStateMachine;
use Kisphp\ShopBundle\StateMachine\StateMachineInterface;

class OrderStateMachineStatusFilter extends AbstractTwigFilter
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'orderStateMachineStatus';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (StateMachineInterface $entity) {
            $sm = new CustomerOrderStateMachine($entity);

            return $sm->getStatusLabel();
        };
    }
}
