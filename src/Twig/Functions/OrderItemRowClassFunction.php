<?php

namespace Kisphp\ShopBundle\Twig\Functions;

use Kisphp\ShopBundle\Entity\AbstractOrder;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\ShopBundle\StateMachine\Declarations\CustomerOrderStateMachine;

class OrderItemRowClassFunction extends AbstractTwigFunction
{
    /**
     * @var array
     */
    protected $statusClass = [
        0 => 'error',
        CustomerOrderStateMachine::STATUS_NEW => 'warning',
        CustomerOrderStateMachine::STATUS_CANCELED => 'danger',
        CustomerOrderStateMachine::STATUS_PROCESSING => 'info',
        CustomerOrderStateMachine::STATUS_DISPACHED => 'info',
        CustomerOrderStateMachine::STATUS_DELIVERED => 'active',
        CustomerOrderStateMachine::STATUS_RETURNED => 'active',
    ];

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'orderItemRowClass';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (AbstractOrder $order) {
            return $this->statusClass[$order->getStatus()];
        };
    }
}
