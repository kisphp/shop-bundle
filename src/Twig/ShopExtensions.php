<?php

namespace Kisphp\ShopBundle\Twig;

use Kisphp\ShopBundle\Twig\Filters\OrderStateMachineStatusFilter;
use Kisphp\ShopBundle\Twig\Filters\PriceFilter;
use Kisphp\ShopBundle\Twig\Functions\OrderItemRowClassFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ShopExtensions extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            OrderStateMachineStatusFilter::create(),
            $this->getPriceFilter(),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            OrderItemRowClassFunction::create(),
        ];
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function getPriceFilter()
    {
        return PriceFilter::create();
    }
}
