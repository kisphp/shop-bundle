<?php

namespace Kisphp\ShopBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\Utils\Status;

class ProductRepository extends EntityRepository
{
    /**
     * @return Article[]
     */
    public function getActiveArticles()
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.status = :status')
            ->setParameter('status', Status::ACTIVE)
        ;

        return $query->getQuery()->getResult();
    }

    /**
     * @param Category $category
     *
     * @return Article[]
     */
    public function getArticlesByCategory(Category $category)
    {
        $query = $this->createQueryBuilder('a')
            ->where('a.id_category = :categ')
            ->setParameter('categ', $category->getId())
        ;

        return $query->getQuery()->getResult();
    }
}
