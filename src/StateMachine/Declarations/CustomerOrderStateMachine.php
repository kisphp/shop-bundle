<?php

namespace Kisphp\ShopBundle\StateMachine\Declarations;

use Kisphp\ShopBundle\StateMachine\StateMachineContext;

class CustomerOrderStateMachine extends StateMachineContext
{
    const STATUS_NEW = 1;
    const STATUS_CANCELED = 2;
    const STATUS_PROCESSING = 3;
    const STATUS_DISPACHED = 4;
    const STATUS_DELIVERED = 5;
    const STATUS_RETURNED = 6;

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $statuses = self::getStates();

        return $statuses[$this->getEntity()->getStatus()][self::STATE_LABEL];
    }

    /**
     * @return array
     */
    protected function getTransitions()
    {
        return [
            'sm.action.process' => [
                'from' => self::STATUS_NEW,
                'to' => self::STATUS_PROCESSING,
            ],
            'sm.action.cancel' => [
                'from' => self::STATUS_NEW,
                'to' => self::STATUS_CANCELED,
            ],
            'sm.action.send' => [
                'from' => self::STATUS_PROCESSING,
                'to' => self::STATUS_DISPACHED,
            ],
            'sm.action.cancel_1' => [
                'from' => self::STATUS_PROCESSING,
                'to' => self::STATUS_CANCELED,
            ],
            'sm.action.arrived' => [
                'from' => self::STATUS_DISPACHED,
                'to' => self::STATUS_DELIVERED,
            ],
            'sm.action.return' => [
                'from' => self::STATUS_DELIVERED,
                'to' => self::STATUS_RETURNED,
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getStates()
    {
        return [
            self::STATUS_NEW => [
                self::STATE_LABEL => 'sm.label.new',
                self::STATE_CAN_CHANGE => false,
            ],
            self::STATUS_CANCELED => [
                self::STATE_LABEL => 'sm.label.canceled',
                self::STATE_CAN_CHANGE => false,
            ],
            self::STATUS_PROCESSING => [
                self::STATE_LABEL => 'sm.label.processing',
                self::STATE_CAN_CHANGE => true,
            ],
            self::STATUS_DISPACHED => [
                self::STATE_LABEL => 'sm.label.dispatched',
                self::STATE_CAN_CHANGE => false,
            ],
            self::STATUS_DELIVERED => [
                self::STATE_LABEL => 'sm.label.delivered',
                self::STATE_CAN_CHANGE => false,
            ],
            self::STATUS_RETURNED => [
                self::STATE_LABEL => 'sm.label.returned',
                self::STATE_CAN_CHANGE => false,
            ],
        ];
    }
}
