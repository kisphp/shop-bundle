<?php

namespace Kisphp\ShopBundle\StateMachine;

abstract class StateMachineContext
{
    const STATUS_NEW = 1;

    const TRANSITION_FROM = 'from';
    const TRANSITION_TO = 'to';

    const STATE_CAN_CHANGE = 'can_change';
    const STATE_LABEL = 'label';

    /**
     * @var StateMachineInterface
     */
    private $entity;

    /**
     * @param $entity
     */
    public function __construct(StateMachineInterface $entity)
    {
        if (empty($entity->getStatus())) {
            $entity->setStatus(static::STATUS_NEW);
        }

        $this->entity = $entity;
    }

    /**
     * @return StateMachineInterface
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return array
     */
    public function getEntityState()
    {
        $statuses = $this->getStates();

        return $statuses[$this->getEntity()->getStatus()];
    }

    /**
     * @return bool
     */
    public function canChange()
    {
        $state = $this->getEntityState();

        if (array_key_exists(self::STATE_CAN_CHANGE, $state) === false) {
            return false;
        }

        return (bool) $state[self::STATE_CAN_CHANGE];
    }

    /**
     * @param $transitionName
     */
    public function changeTransition($transitionName)
    {
        $transitions = $this->getTransitions();
        if (array_key_exists($transitionName, $transitions)) {
            $state = $transitions[$transitionName];
            if ($this->entity->getStatus() === $state[self::TRANSITION_FROM]) {
                $this->entity->setStatus($state[self::TRANSITION_TO]);
            }
        }
    }

    /**
     * @return array
     */
    public function getAvailableTransitions()
    {
        $transitions = [];
        foreach ($this->getTransitions() as $transitionName => $settings) {
            if ($settings[self::TRANSITION_FROM] !== $this->entity->getStatus()) {
                continue;
            }
            $transitions[$transitionName] = $settings;
        }

        return $transitions;
    }

    /**
     * @return array
     */
    abstract protected function getTransitions();

    /**
     * @return array
     */
    abstract protected function getStates();
}
