<?php

namespace Kisphp\ShopBundle\StateMachine;

interface StateMachineInterface
{
    /**
     * @param int $status
     */
    public function setStatus($status);

    /**
     * @return int
     */
    public function getStatus();
}
