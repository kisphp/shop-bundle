<?php

namespace Kisphp\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Kisphp\Utils\Strings;

/**
 * @ORM\Table(name="products", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\Entity(repositoryClass="Kisphp\ShopBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Product implements KisphpEntityInterface, ToggleableInterface, PriceInterface, PriceOldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_category;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $item_url;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, length=32)
     */
    protected $code;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var ProductCategory
     *
     * @ORM\ManyToOne(targetEntity="ProductCategory", inversedBy="products")
     * @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     */
    protected $category;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    protected $available_stoc;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $price;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    protected $price_old;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_keywords;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seo_description;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $body;

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        if ($this->registered === null) {
            $this->setRegistered(new \DateTime());
        }
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setItemUrl($title);

        if (empty($this->getSeoTitle())) {
            $this->setSeoTitle($title);
        }
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdCategory()
    {
        return $this->id_category;
    }

    /**
     * @param int $id_category
     */
    public function setIdCategory($id_category)
    {
        $this->id_category = $id_category;
    }

    /**
     * @return ProductCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param ProductCategory $category
     */
    public function setCategory(ProductCategory $category)
    {
        $this->category = $category;
        if ($category->getId() > 0) {
            $this->id_category = $category->getId();
        }
    }

    /**
     * @return string
     */
    public function getItemUrl()
    {
        return $this->item_url;
    }

    /**
     * @param string $item_url
     */
    public function setItemUrl($item_url)
    {
        $this->item_url = Strings::niceUrlTitle($item_url);
    }

    /**
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * @param string $seo_title
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;
    }

    /**
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * @param string $seo_keywords
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * @param string $seo_description
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;
    }

    /**
     * @return int
     */
    public function getAvailableStoc()
    {
        return $this->available_stoc;
    }

    /**
     * @param int $available_stoc
     */
    public function setAvailableStoc($available_stoc)
    {
        $this->available_stoc = $available_stoc;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPriceOld()
    {
        return $this->price_old;
    }

    /**
     * @param int $price_old
     */
    public function setPriceOld($price_old)
    {
        $this->price_old = $price_old;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
