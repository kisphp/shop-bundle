<?php

namespace Kisphp\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Utils\Status;

/**
 * @ORM\Table(name="orders_items", options={"colate": "utf8_general_ci", "charset": "utf8"})
 * @ORM\HasLifecycleCallbacks()
 * @ORM\MappedSuperclass()
 */
abstract class AbstractOrderItem implements KisphpEntityInterface, PriceInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_order;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": Kisphp\Utils\Status::ACTIVE})
     */
    protected $status = Status::INACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_product;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $price;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $tax_value = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $shippingCost = 0;

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        if ($this->registered === null) {
            $this->setRegistered(new \DateTime());
        }
    }

    /**
     * @return float|int
     */
    public function getTotalPrice()
    {
        $itemPrice = $this->quantity * $this->price;
        $itemPrice += $itemPrice * $this->tax_value / 100;
        $itemPrice += $itemPrice * $this->shippingCost / 100;

        return $itemPrice;
    }
}
