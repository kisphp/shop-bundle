<?php

namespace Kisphp\ShopBundle\Entity;

interface PriceInterface
{
    /**
     * @return int
     */
    public function getPrice();

    /**
     * @param int $price
     */
    public function setPrice($price);
}
