<?php

namespace Kisphp\ShopBundle\Entity;

interface PriceOldInterface
{
    /**
     * @return int
     */
    public function getPriceOld();

    /**
     * @param int $price
     */
    public function setPriceOld($price);
}
