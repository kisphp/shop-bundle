<?php

namespace Kisphp\ShopBundle\Controller;

use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\ShopBundle\Form\CustomerOrderItemCreateForm;
use Kisphp\ShopBundle\Form\CustomerOrderItemForm;
use Symfony\Component\HttpFoundation\Request;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;
use AppBundle\Entity\OrderItem;

class OrderItemsController extends AbstractController
{
    const SECTION_TITLE = 'section.title.shop.customer_orders';
    const MODEL_NAME = 'model.shop.order.item';
    const ENTITY_FORM_CLASS = CustomerOrderItemForm::class;
    const EDIT_TEMPLATE = '@Shop/Orders/edit-item.html.twig';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'shop_orders_edit',
        self::LIST_PATH => 'shop_orders',
        self::ADD_PATH => 'shop_orders_add',
        self::STATUS_PATH => 'shop_orders_status',
        self::DELETE_PATH => 'shop_orders_delete',
    ];

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAction(Request $request, $id)
    {
        $model = $this->get('model.shop.order.item');

        $order = $this->get('model.shop.order')->find($id);

        $entity = $model->createEntity();
        $entity->setQuantity(1);

        $form = $this->createFormObjectAddItem($request, $entity);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var OrderItem $orderItem */
            $orderItem = $form->getData();
            $product = $this->get('model.shop.product')->find($orderItem->getIdProduct());
            $model->addItemToOrder($form->getData(), $product, $order);

            return $this->redirect(
                $this->generateUrl('shop_orders_total', [
                    'id' => $id,
                ])
            );
        }

        return $this->render(
            static::EDIT_TEMPLATE,
            [
                'id' => $id,
                'entity' => $entity,
                'form' => $form->createView(),
                'attached' => $this->getAttachedFiles($id),
                'section' => $this->section,
                'html_editor' => static::HTML_EDITOR_ENABLED,
                'upload_type' => static::UPLOAD_TYPE,
                'images_list_url' => $this->getImagesEditUrl($id),
                'templates' => $this->get('model.cms.template')->getTemplatesFormEditor(),
                'section_title' => static::SECTION_TITLE . (($id > 0) ? '.edit' : '.create'),
            ]
        );
    }

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function totalAction($id)
    {
        $this->get('model.shop.order')->recalculateTotal($id);

        return $this->redirect(
            $this->generateUrl('shop_orders_edit', ['id' => $id])
        );
    }

    /**
     * @param KisphpEntityInterface $entity
     * @param string $message
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectAfterSave(KisphpEntityInterface $entity, $message = 'Successfull saved')
    {
        $this->addFlash('success', 'Product item was successfully saved');

        return $this->redirect(
            $this->generateUrl('shop_orders_total', [
                'id' => $entity->getIdOrder(),
            ])
        );
    }


    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObjectAddItem(Request $request, $object)
    {
        $form = $this->createForm(CustomerOrderItemCreateForm::class, $object);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $form = $this->createForm(CustomerOrderItemForm::class, $object);
        $form->handleRequest($request);

        return $form;
    }
}
