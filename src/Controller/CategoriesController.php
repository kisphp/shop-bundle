<?php

namespace Kisphp\ShopBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Kisphp\ShopBundle\Form\CategoryForm;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;

/**
 * @Template()
 */
class CategoriesController extends AbstractController
{
    const SECTION_TITLE = 'section.title.categories';
    const MODEL_NAME = 'model.shop.product.category';
    const ENTITY_FORM_CLASS = CategoryForm::class;
    const LISTING_TEMPLATE = '@Shop/Categories/index.html.twig';

    const HTML_EDITOR_ENABLED = true;
    const UPLOAD_TYPE = 'product_category';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'shop_categories_edit',
        self::LIST_PATH => 'shop_categories',
        self::ADD_PATH => 'shop_categories_add',
        self::STATUS_PATH => 'shop_categories_status',
        self::DELETE_PATH => 'shop_categories_delete',
    ];

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $items = $this->get(static::MODEL_NAME)
            ->getCategoriesForListing()
        ;

        return $this->render(
            static::LISTING_TEMPLATE,
            [
                'section_title' => static::SECTION_TITLE . '.list',
                'items' => $items,
                'section' => $this->section,
            ]
        );
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return FormInterface|\Symfony\Component\Form\Form
     */
    protected function createFormObject(Request $request, $object)
    {
        $form = $this->createForm(static::ENTITY_FORM_CLASS, $object, [
            'categories_choices' => $this->get('model.shop.product.category')->getCategoriesForForm(),
        ]);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @return string
     *
     * @param mixed $id
     */
    protected function getImagesEditUrl($id)
    {
        return $this->generateUrl('shop_categories_attached', [
            'id' => $id,
        ]);
    }

    /**
     * @param int $articleId
     *
     * @return array
     */
    protected function getAttachedFiles($articleId)
    {
        $model = $this->get('model.media_files');

        return $model->findBy([
            'id_object' => $articleId,
            'object_type' => static::UPLOAD_TYPE,
        ]);
    }
}
