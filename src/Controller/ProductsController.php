<?php

namespace Kisphp\ShopBundle\Controller;

use Kisphp\ShopBundle\Form\ProductForm;
use Symfony\Component\HttpFoundation\Request;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;

class ProductsController extends AbstractController
{
    const SECTION_TITLE = 'section.title.shop.products';
    const MODEL_NAME = 'model.shop.product';
    const LISTING_TEMPLATE = '@Shop/Products/index.html.twig';
    const ENTITY_FORM_CLASS = ProductForm::class;

    const HTML_EDITOR_ENABLED = true;
    const UPLOAD_TYPE = 'product';

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'shop_products_edit',
        self::LIST_PATH => 'shop_products',
        self::ADD_PATH => 'shop_products_add',
        self::STATUS_PATH => 'shop_products_status',
        self::DELETE_PATH => 'shop_products_delete',
    ];

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $model = $this->get('model.shop.product.category');
        $form = $this->createForm(static::ENTITY_FORM_CLASS, $object, [
            'categories_choices' => $model->getCategoriesForForm(),
            'category_model' => $model,
        ]);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param int $objectId
     *
     * @return array
     */
    protected function getAttachedFiles($objectId)
    {
        $model = $this->get('model.media_files');

        return $model->findBy([
            'id_object' => $objectId,
            'object_type' => static::UPLOAD_TYPE,
        ]);
    }
}
