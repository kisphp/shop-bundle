<?php

namespace Kisphp\ShopBundle\Controller;

use Kisphp\ShopBundle\Entity\AbstractOrder;
use Kisphp\ShopBundle\Form\CustomerOrderForm;
use Kisphp\ShopBundle\StateMachine\Declarations\CustomerOrderStateMachine;
use Symfony\Component\HttpFoundation\Request;
use Kisphp\FrameworkAdminBundle\Controller\AbstractController;

class OrdersController extends AbstractController
{
    const SECTION_TITLE = 'section.title.shop.customer_orders';
    const MODEL_NAME = 'model.shop.order';
    const LISTING_TEMPLATE = '@Shop/Orders/index.html.twig';
    const ENTITY_FORM_CLASS = CustomerOrderForm::class;

    /**
     * @var array
     */
    protected $section = [
        self::EDIT_PATH => 'shop_orders_edit',
        self::LIST_PATH => 'shop_orders',
        self::ADD_PATH => 'shop_orders_add',
        self::STATUS_PATH => 'shop_orders_status',
        self::DELETE_PATH => 'shop_orders_delete',
    ];

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        /** @var AbstractOrder $entity */
        $entity = $this->get('model.shop.order')->find($id);
        $form = $this->createEditForm($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSubmittedForm($form);
        }

        $ids = [];
        foreach ($entity->getOrderItems() as $item) {
            $ids[] = $item->getIdProduct();
        }

        $products = $this->get('model.shop.product')->getProductsByIdList($ids);

        $stateMachine = new CustomerOrderStateMachine($entity);

        return $this->render(
            '@Shop/Orders/edit.html.twig',
            [
                'id' => $id,
                'stateMachine' => $stateMachine,
                'entity' => $entity,
                'products' => $products,
                'form' => $form->createView(),
                'attached' => $this->getAttachedFiles($id),
                'section' => $this->section,
                'html_editor' => static::HTML_EDITOR_ENABLED,
                'upload_type' => static::UPLOAD_TYPE,
                'images_list_url' => $this->getImagesEditUrl($id),
                'templates' => $this->get('model.cms.template')->getTemplatesFormEditor(),
                'section_title' => static::SECTION_TITLE . (($id > 0) ? '.edit' : '.create'),
            ]
        );
    }

    /**
     * @param int $id
     * @param string $state
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeStateAction($id, $state)
    {
        $model = $this->get('model.shop.order');
        $order = $model->find($id);

        $sm = new CustomerOrderStateMachine($order);

        $sm->changeTransition($state);

        $model->save($order);

        return $this->redirect(
            $this->generateUrl('shop_orders_edit', [
                'id' => $order->getId(),
            ])
        );
    }

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function totalAction($id)
    {
        $this->get('model.shop.order')->recalculateTotal($id);

        return $this->redirect(
            $this->generateUrl('shop_orders_edit', ['id' => $id])
        );
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $form = $this->createForm(CustomerOrderForm::class, $object);
        $form->handleRequest($request);

        return $form;
    }
}
