<?php

namespace Kisphp\ShopBundle\DemoData;

use Faker\Factory;
use Kisphp\FrameworkAdminBundle\Fixtures\AbstractDemoData;
use Kisphp\Utils\Strings;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;

class ShopDemo extends AbstractDemoData
{
    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function loadDemoData(InputInterface $input, OutputInterface $output)
    {
        $this->createCategories($output);
        $this->createProducts($output);
        $this->createDemoOrder($output);
    }

    /**
     * @param OutputInterface $output
     */
    protected function createProducts(OutputInterface $output)
    {
        $faker = Factory::create();
        $model = $this->getContainer()->get('model.shop.product');

        $cat = $this->getContainer()->get('model.shop.product.category');

        /** @var \Kisphp\ArticlesBundle\Entity\Category $category */
        $category = $cat->findOneBy([
            'item_url' => 'shop-category-1',
        ]);

        /** @var \Kisphp\ShopBundle\Entity\Product $art */
        $art = $model->createEntity();
        $art->setTitle('Product 1');
        $art->setBody($faker->text(300));
        $art->setCategory($category);
        $art->setPrice(200);
        $art->setCode('prod-1');
        $art->setAvailableStoc(10);

        $model->persist($art);

        $output->writeln(sprintf('Created product: <info>%s</info>', $art->getTitle()));

        $model->flush();
    }

    /**
     * @param OutputInterface $output
     */
    protected function createCategories(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.shop.product.category');

        // 1
        $cat = $model->createCategoryEntity('shop category 1');
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        // 2
        $cat = $model->createCategoryEntity('shop category 2');
        $cat->setItemUrl(Strings::niceUrlTitle($cat->getTitle()));
        $cat->setSeoTitle($cat->getTitle());
        $model->persist($cat);

        $model->flush();
        $output->writeln('<info>Created shop categories</info>');
    }

    protected function createDemoOrder(OutputInterface $output)
    {
        $model = $this->getContainer()->get('model.shop.order');

        $request = Request::createFromGlobals();
        $orderItemsModel = $this->getContainer()->get('model.shop.order.item');

        /** @var \AppBundle\Entity\Order $order */
        $order = $model->createEntity();
        $order->setName('customer 1');
        $order->setEmail('test@example.com');
        $order->setCity('city');
        $order->setAddress('address');
        $order->setCompanyName('company');

        $model->save($order);

        /** @var \AppBundle\Entity\OrderItem $orderItem */
        $orderItem = $orderItemsModel->createEntity();
        $orderItem->setPrice(200);
        $orderItem->setIdProduct(1);
        $orderItem->setQuantity(100);
        $orderItem->setOrder($order);

        $orderItemsModel->save($orderItem);

        $output->writeln('<info>Created shop demo order</info>');
    }
}
