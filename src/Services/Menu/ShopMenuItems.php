<?php

namespace Kisphp\ShopBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItemInterface;

class ShopMenuItems implements MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'shop',
            'label' => 'Shop',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'shop',
            'match_route' => 'products',
            'path' => 'shop_products',
            'icon' => 'fa-list',
            'label' => 'shop_navigation.products',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'shop',
            'match_route' => 'shop_categories',
            'path' => 'shop_categories',
            'icon' => 'fa-tree',
            'label' => 'shop_navigation.products_categories',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'shop',
            'match_route' => 'orders',
            'path' => 'shop_orders',
            'icon' => 'fa-shopping-bag',
            'label' => 'shop_navigation.customer_orders',
        ]);
    }
}
