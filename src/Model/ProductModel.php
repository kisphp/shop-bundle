<?php

namespace Kisphp\ShopBundle\Model;

use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Kisphp\ShopBundle\Entity\Product;

class ProductModel extends AbstractModel implements MediaModelInterface
{
    const REPOSITORY = 'ShopBundle:Product';

    /**
     * @return string
     */
    public function getMediaType()
    {
        return 'product';
    }

    /**
     * @return Product
     */
    public function createEntity()
    {
        return new Product();
    }

    /**
     * @param int[] $ids
     *
     * @return array
     */
    public function getProductsByIdList(array $ids)
    {
        $collection = $this->getRepository()->createQueryBuilder('o')
            ->where('o.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
        ;

        $products = [];
        /** @var \Kisphp\ShopBundle\Entity\AbstractOrderItem $item */
        foreach ($collection as $item) {
            $products[$item->getId()] = $item;
        }

        return $products;
    }
}
