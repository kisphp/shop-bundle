<?php

namespace Kisphp\ShopBundle\Model;

use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\ShopBundle\Entity\AbstractOrder;

abstract class AbstractCustomerOrderModel extends AbstractModel
{
    /**
     * @param int $id
     */
    public function recalculateTotal($id)
    {
        /** @var AbstractOrder $entity */
        $entity = $this->getRepository()->find($id);

        if ($entity === null) {
            return;
        }

        $this->calculateTotalPrice($entity);

        $this->save($entity);
    }

    /**
     * @param AbstractOrder $entity
     *
     * @return $this
     */
    public function calculateTotalPrice(AbstractOrder $entity)
    {
        $totalPrice = 0;
        /** @var \Kisphp\ShopBundle\Entity\AbstractOrderItem $item */
        foreach ($entity->getOrderItems() as $item) {
            $totalPrice += $item->getTotalPrice();
        }

        $entity->setTotalPrice($totalPrice);

        return $this;
    }
}
