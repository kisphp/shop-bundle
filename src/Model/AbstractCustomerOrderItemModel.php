<?php

namespace Kisphp\ShopBundle\Model;

use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\ShopBundle\Entity\AbstractOrder;
use Kisphp\ShopBundle\Entity\AbstractOrderItem;
use Kisphp\ShopBundle\Entity\Product;

abstract class AbstractCustomerOrderItemModel extends AbstractModel
{
    /**
     * @param AbstractOrderItem $orderItem
     * @param AbstractOrder $order
     */
    public function addItemToOrder(AbstractOrderItem $orderItem, Product $product, AbstractOrder $order)
    {
        $orderItem->setOrder($order);
        $orderItem->setPrice($product->getPrice());

        $this->save($orderItem);
    }
}
