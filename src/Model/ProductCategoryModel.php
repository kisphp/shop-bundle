<?php

namespace Kisphp\ShopBundle\Model;

use Doctrine\ORM\EntityRepository;
use Kisphp\FrameworkAdminBundle\Model\AbstractModel;
use Kisphp\MediaBundle\Model\MediaModelInterface;
use Kisphp\ShopBundle\Entity\ProductCategory;
use Kisphp\Utils\Status;

/**
 * @method EntityRepository getRepository()
 */
class ProductCategoryModel extends AbstractModel implements MediaModelInterface
{
    const REPOSITORY = 'ShopBundle:ProductCategory';

    /**
     * @return string
     */
    public function getMediaType()
    {
        return 'product_category';
    }

    /**
     * @return Category
     *
     * @param null|mixed $categoryTitle
     */
    public function createCategoryEntity($categoryTitle = null)
    {
        /** @var Category $cat */
        $cat = $this->createEntity();

        if ($categoryTitle !== null) {
            $cat->setTitle($categoryTitle);
        }
        $cat->setItemUrl($categoryTitle);

        return $cat;
    }

    /**
     * @return array
     */
    public function getActiveServices()
    {
        return $this->getRepository()->getActiveServices();
    }

    /**
     * @return array
     */
    public function getCategoriesForForm()
    {
        $level = 0;
        $categories = [];
        $this->getCategoriesChildren($categories, 0, $level);

        return $categories;
    }

    /**
     * @return array
     */
    public function getCategoriesForListing()
    {
        $level = 0;
        $categories = [];
        $this->getCategoryEntitiesChildren($categories, 0, $level);

        return $categories;
    }

    /**
     *  return array.
     */
    public function getCategoriesWithChildren()
    {
        return $this->getCategoriesTreeByParentId(3);
    }

    /**
     * @param int $parentId
     *
     * @return array
     */
    public function getCategoriesTreeByParentId($parentId)
    {
        $categories = [];
        $list = $this->getRepository()->getCategoriesByParentId($parentId, [
            Status::ACTIVE,
        ]);
        /** @var Category $cat */
        foreach ($list as $cat) {
            $catId = $cat->getId();
            $categories[$catId] = [
                'id' => $catId,
                'title' => $cat->getTitle(),
                'item_url' => $cat->getItemUrl(),
                'status' => $cat->getStatus(),
                'children' => $this->getCategoriesTreeByParentId($catId),
            ];
        }

        return $categories;
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    public function toArrayForMenu(Category $category)
    {
        return [
            'id' => $category->getId(),
            'title' => $category->getTitle(),
            'status' => $category->getStatus(),
            'item_url' => $category->getItemUrl(),
        ];
    }

    /**
     * @return ProductCategory
     */
    public function createEntity()
    {
        return new ProductCategory();
    }

    /**
     * @param array $categories
     * @param int $parentId
     * @param mixed $level
     */
    protected function getCategoriesChildren(array &$categories, $parentId, $level)
    {
        $list = $this->getRepository()
            ->getCategoriesByParentId(
                $parentId,
                [
                Status::ACTIVE,
                Status::INACTIVE,
            ]
        );
        /** @var Category $cat */
        foreach ($list as $cat) {
            $key = '';
            if ($level > 0) {
                $key .= str_repeat('-', $level) . ' ';
            }
            $key .= $cat->getTitle();

            $categories[$key] = $cat->getId();
            $this->getCategoriesChildren($categories, $cat->getId(), $level + 1);
        }
    }

    /**
     * @param array $categories
     * @param int $parentId
     * @param mixed $level
     */
    protected function getCategoryEntitiesChildren(array &$categories, $parentId, $level)
    {
        $list = $this->getRepository()
            ->getCategoriesByParentId(
                $parentId,
                [
                Status::ACTIVE,
                Status::INACTIVE,
            ]
        );
        /** @var Category $cat */
        foreach ($list as $cat) {
            $categories[$cat->getId()] = [
                'level' => $level,
                'object' => $cat,
            ];
            $this->getCategoryEntitiesChildren($categories, $cat->getId(), $level + 1);
        }
    }
}
