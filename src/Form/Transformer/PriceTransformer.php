<?php

namespace Kisphp\ShopBundle\Form\Transformer;

use Kisphp\ShopBundle\Entity\PriceInterface;

class PriceTransformer
{
    /**
     * @param PriceInterface|\Kisphp\ShopBundle\Entity\PriceOldInterface $entity
     *
     * @return \Kisphp\ShopBundle\Entity\PriceInterface|\Kisphp\ShopBundle\Entity\PriceOldInterface
     */
    public static function databaseToForm(PriceInterface $entity)
    {
        $entity->setPrice($entity->getPrice() / 100);
        if (method_exists($entity, 'setPriceOld')) {
            $entity->setPriceOld($entity->getPriceOld() / 100);
        }

        return $entity;
    }

    /**
     * @param PriceInterface|\Kisphp\ShopBundle\Entity\PriceOldInterface $entity
     *
     * @return \Kisphp\ShopBundle\Entity\PriceInterface|\Kisphp\ShopBundle\Entity\PriceOldInterface
     */
    public static function formToDatabase(PriceInterface $entity)
    {
        $entity->setPrice($entity->getPrice() * 100);
        if (method_exists($entity, 'setPriceOld')) {
            $entity->setPriceOld($entity->getPriceOld() * 100);
        }

        return $entity;
    }
}
