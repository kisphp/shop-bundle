<?php

namespace Kisphp\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerOrderForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('customer_type', ChoiceType::class, [
            'expanded' => true,
            'choices' => [
                'shop.form.customer_type.pf' => 'pf',
                'shop.form.customer_type.pj' => 'pj',
            ],
            'attr' => [
                'class' => 'input-choice',
            ],
        ]);
        $builder->add('name');
        $builder->add('email');
        $builder->add('phone');
        $builder->add('city');
        $builder->add('address');
        $builder->add('comments', TextareaType::class);
    }
}
