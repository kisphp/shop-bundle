<?php

namespace Kisphp\ShopBundle\Form;

use Kisphp\ShopBundle\Entity\ProductCategory;
use Kisphp\Utils\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CategoryForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => ProductCategory::class,
        ]);

        $resolver->setRequired('categories_choices');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('status', ChoiceType::class, [
            'expanded' => true,
            'choices' => [
                'status.active' => Status::ACTIVE,
                'status.inactive' => Status::INACTIVE,
            ],
            'attr' => [
                'class' => 'input-choice',
            ],
        ]);
        $builder->add('id_parent', ChoiceType::class, [
            'label' => 'Parent Category',
            'choices' => array_merge(
                [
                    'form.choices.no_sparent_category' => 0,
                ],
                $options['categories_choices']
            ),
            'empty_data' => null,
        ]);
        $builder->add('description', TextareaType::class, [
            'label' => 'Category content',
            'attr' => [
                'class' => 'html-edit',
            ],
        ]);
        $builder->add('seo_title');
        $builder->add('seo_keywords');
        $builder->add('seo_description', TextareaType::class);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                return $this->transformFromDatabase($entity);
            },
            /**
             * @param Category $entity
             */
            function ($entity) {
                return $this->prepareForDatabase($entity);
            }
        ));
    }

    /**
     * @param ProductCategory $entity
     *
     * @return ProductCategory
     */
    protected function transformFromDatabase(ProductCategory $entity)
    {
        return $entity;
    }

    /**
     * @param ProductCategory $entity
     *
     * @return ProductCategory
     */
    protected function prepareForDatabase(ProductCategory $entity)
    {
        if (empty($entity->getSeoTitle())) {
            $entity->setSeoTitle($entity->getTitle());
        }
        $entity->setItemUrl($entity->getTitle());

        return $entity;
    }
}
