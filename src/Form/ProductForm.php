<?php

namespace Kisphp\ShopBundle\Form;

use Kisphp\ShopBundle\Entity\Product;
use Kisphp\Utils\Status;
use Kisphp\ShopBundle\Form\Transformer\PriceTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductForm extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'data_class' => Product::class,
        ]);

        $resolver->setRequired('categories_choices');
        $resolver->setRequired('category_model');
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id_category', ChoiceType::class, [
            'label' => 'Category',
            'choices' => array_merge(
                [
//                    'form.choices.no_sparent_category' => 0,
                ],
                $options['categories_choices'
            ]
            ),
            'empty_data' => null,
        ]);
        $builder->add('title', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('code', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('body', TextareaType::class, [
            'attr' => [
                'class' => 'html-edit',
            ],
        ]);
        $builder->add('seo_title');
        $builder->add('seo_keywords');
        $builder->add('seo_description');
        $builder->add('available_stoc', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('price', NumberType::class, [
            'constraints' => [
                new NotBlank(),
            ],
        ]);
        $builder->add('price_old', TextType::class);
        $builder->add('status', ChoiceType::class, [
            'expanded' => true,
            'choices' => [
                'status.active' => Status::ACTIVE,
                'status.inactive' => Status::INACTIVE,
            ],
            'attr' => [
                'class' => 'input-choice',
            ],
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                return $this->displayEntityInForm($entity);
            },
            function ($entity) use ($options) {
                return $this->prepareEntityForDatabase($entity, $options);
            }
        ));
    }

    /**
     * @param \Kisphp\ShopBundle\Entity\PriceInterface|Product $entity
     *
     * @return Product
     */
    protected function displayEntityInForm(Product $entity)
    {
        PriceTransformer::databaseToForm($entity);

        return $entity;
    }

    /**
     * @param \Kisphp\ShopBundle\Entity\PriceInterface|Product $entity
     * @param array $options
     *
     * @return Product
     */
    protected function prepareEntityForDatabase(Product $entity, array $options)
    {
        /** @var \Kisphp\ShopBundle\Model\ProductCategoryModel $model */
        $model = $options['category_model'];

        /** @var \Kisphp\ShopBundle\Entity\ProductCategory $categ */
        $categ = $model->find($entity->getIdCategory());
        $entity->setCategory($categ);

        PriceTransformer::formToDatabase($entity);

        if (empty($entity->getSeoTitle())) {
            $entity->setSeoTitle($entity->getTitle());
        }

        $entity->setItemUrl($entity->getTitle());

        return $entity;
    }
}
