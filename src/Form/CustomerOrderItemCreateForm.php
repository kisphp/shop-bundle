<?php

namespace Kisphp\ShopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class CustomerOrderItemCreateForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id_product');
        $builder->add('quantity');
        $builder->add('tax_value', NumberType::class, [
            'constraints' => [
                new GreaterThanOrEqual([
                    'value' => 0,
                ]),
                new LessThanOrEqual([
                    'value' => 100,
                ]),
            ],
        ]);
        $builder->add('shippingCost', NumberType::class, [
            'constraints' => [
                new GreaterThanOrEqual([
                    'value' => 0,
                ]),
                new LessThanOrEqual([
                    'value' => 100,
                ]),
            ],
        ]);
    }
}
