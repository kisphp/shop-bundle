<?php

namespace Kisphp\ShopBundle\Form;

use Kisphp\ShopBundle\Entity\PriceInterface;
use Kisphp\ShopBundle\Form\Transformer\PriceTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class CustomerOrderItemForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id_product');
        $builder->add('quantity');
        $builder->add('price', TextType::class);
        $builder->add('tax_value', NumberType::class, [
            'constraints' => [
                new GreaterThanOrEqual([
                    'value' => 0,
                ]),
                new LessThanOrEqual([
                    'value' => 100,
                ]),
            ],
        ]);
        $builder->add('shippingCost', NumberType::class, [
            'constraints' => [
                new GreaterThanOrEqual([
                    'value' => 0,
                ]),
                new LessThanOrEqual([
                    'value' => 100,
                ]),
            ],
        ]);

        $builder->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                $this->displayEntityInForm($entity);

                return $entity;
            },
            function ($entity) {
                return $this->prepareEntityForDatabase($entity);
            }
        ));
    }

    /**
     * @param PriceInterface $entity
     *
     * @return PriceInterface
     */
    protected function displayEntityInForm(PriceInterface $entity)
    {
        PriceTransformer::databaseToForm($entity);

        return $entity;
    }

    /**
     * @param PriceInterface $entity
     *
     * @return PriceInterface
     */
    protected function prepareEntityForDatabase(PriceInterface $entity)
    {
        PriceTransformer::formToDatabase($entity);

        return $entity;
    }
}
